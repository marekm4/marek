<?php if ($files): ?>
    <?php foreach ($files as $file): ?>
        <a href="<?php echo get_path($dir); ?><?php echo $file; ?>"><?php echo $file; ?></a>
        <?php if ($admin): ?>
            <a href="panel.php?dir=<?php echo $dir; ?>&amp;file=<?php echo urlencode($file); ?>" class="edit">[Edit]</a>
            <a href="file.move.php?csrf=<?php echo $csrf; ?>&amp;from=<?php echo $dir; ?>&amp;file=<?php echo urlencode($file); ?>" class="move">[Move]</a>
            <a href="file.delete.php?csrf=<?php echo $csrf; ?>&amp;dir=<?php echo $dir; ?>&amp;file=<?php echo urlencode($file); ?>" class="delete">[Delete]</a>
        <?php endif; ?>
        <br>
    <?php endforeach; ?>
<?php endif; ?>
