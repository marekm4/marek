<form action="file.add.php" method="post" enctype="multipart/form-data">
    <input type="hidden" name="csrf" value="<?php echo $csrf; ?>">
    <input type="hidden" name="MAX_FILE_SIZE" value="10485760">
    <label>File:</label>
    <input type="file" name="file">
    <br>
    <label>Address:</label>
    <input type="text" name="address">
    <br>
    <label>Name:</label>
    <input type="text" name="name" value="<?php echo $file['name']; ?>">
    <br>
    <label>Content:</label>
    <textarea name="content"><?php echo $file['content']; ?></textarea>
    <br>
    <label>Private:</label>
    <input type="checkbox" name="private" value="1" <?php if($file['private']): ?>checked<?php endif; ?>>
    <br>
    <label>Salt:</label>
    <input type="checkbox" name="salt" value="1">
    <br>
    <label>&nbsp;</label>
    <input type="submit" value="Add file" class="button">
</form>
Public:
<br>
<?php $dir = $public['dir']; ?>
<?php $files = $public['files']; ?>
<?php require 'files.php'; ?>
Private:
<br>
<?php $dir = $private['dir']; ?>
<?php $files = $private['files']; ?>
<?php require 'files.php'; ?>
