<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title><?php echo $name; ?> - <?php echo $title; ?></title>
        <link rel="stylesheet" href="css/main.css">
    </head>
    <body>
        <div id="menu">
            <a href="index.php">Files</a>
            <a href="library.show.php">Library</a>
            <?php if ($admin): ?>
                <a href="panel.php">Panel</a>
                <a href="logout.php">Logout</a>
            <?php else: ?>
                <a href="login.php">Login</a>
            <?php endif; ?>
        </div>
        <div id="content">
            <?php require_once __DIR__ . DIRECTORY_SEPARATOR . $view . '.php'; ?>
        </div>
    </body>
</html>
