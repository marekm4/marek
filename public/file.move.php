<?php

require_once '../functions.php';
ensure_admin();

$from = $_GET['from'];
if (csrf_token() == $_GET['csrf'] && in_array($from, get_dirs())) {
    $to = $from === get_dirs()[0] ? get_dirs()[1] : get_dirs()[0];
    rename(get_path($from) . basename($_GET['file']), get_path($to) . basename($_GET['file']));
}

http_redirect('panel.php');
