<?php

require_once '../functions.php';
ensure_admin();

$page = page_data();

$page['title'] = 'Panel';

$page['public']['dir'] = 'public';
$page['public']['files'] = get_files(get_path($page['public']['dir']));
$page['private']['dir'] = 'private';
$page['private']['files'] = get_files(get_path($page['private']['dir']));

$page['file']['name'] = '';
$page['file']['content'] = '';
$page['file']['private'] = false;

if (isset($_GET['file'])) {
    $page['file']['name'] = $_GET['file'];
    $page['file']['content'] = file_get_contents('files' . DIRECTORY_SEPARATOR . $_GET['dir'] . DIRECTORY_SEPARATOR . $_GET['file']);
    $page['file']['private'] = $_GET['dir'] === get_dirs()[1];
}

html_template('panel', $page);
