<?php

require_once '../functions.php';
ensure_admin();

if ($_POST['csrf'] == csrf_token()) {
    //determine directory
    $dir = get_path('public');
    if (isset($_POST['private'])) {
        $dir = get_path('private');
    }

    //determine file name
    if (strlen($_FILES['file']['name'])) {
        $name = $_FILES['file']['name'];
    }
    if (strlen($_POST['address'])) {
        $name = basename(parse_url($_POST['address'], PHP_URL_PATH));
        $extension = pathinfo($name, PATHINFO_EXTENSION);
        if (strlen($name) <= 1) {
            $name = parse_url($_POST['address'], PHP_URL_HOST);
        }
        if (strlen($extension) == 0) {
            $name .= '.html';
        }
    }
    if (strlen($_POST['name'])) {
        $name = $_POST['name'];
    }

    //add salt
    if (isset($_POST['salt'])) {
        $salt = md5(rand(1, 1e9) * time());
        $pathinfo = pathinfo($name);
        $name = $pathinfo['filename'] . '_' . $salt . '.' . $pathinfo['extension'];
    }

    //determine file content
    if (strlen($_FILES['file']['name'])) {
        move_uploaded_file($_FILES['file']['tmp_name'], $dir . $name);
    } elseif (strlen($_POST['address'])) {
        file_put_contents($dir . $name, file_get_contents($_POST['address']));
    } elseif (strlen($_POST['content'])) {
        file_put_contents($dir . $name, stripslashes($_POST['content']));
    }
}

http_redirect('panel.php');
