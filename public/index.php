<?php

require_once '../functions.php';

$page = page_data();

$page['title'] = 'Files';

$page['dir'] = 'public';
$page['files'] = get_files(get_path($page['dir']));

html_template('files', $page);
