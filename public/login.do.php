<?php

require_once '../functions.php';

$password = $_POST['password'];

if (password_verify($password, $_SERVER['PASSWORD'])) {
    $_SESSION['admin'] = true;
    http_redirect('panel.php');
}

http_redirect('login.php');
