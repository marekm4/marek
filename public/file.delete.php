<?php

require_once '../functions.php';
ensure_admin();

$dir = $_GET['dir'];
if (csrf_token() == $_GET['csrf'] && in_array($dir, get_dirs())) {
    unlink(get_path($dir) . basename($_GET['file']));
}

http_redirect('panel.php');
