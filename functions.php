<?php

session_start();

require_once 'library.php';
envs_load();

function is_admin() {
    return isset($_SESSION['admin']) ? $_SESSION['admin'] : false;
}

function page_data() {
    $data['name'] = 'Marek';
    $data['admin'] = is_admin();
    $data['csrf'] = csrf_token();

    return $data;
}

function ensure_admin() {
    if (!is_admin()) {
        http_redirect('login.php');
    }
}

function get_path($type) {
    return 'files/' . $type . '/';
}

function get_dirs() {
    return ['public', 'private'];
}

function get_files($dir) {
    $list = [];
    $handle = opendir($dir);
    while ($file = readdir($handle)) {
        if (!in_array($file, array('.', '..', 'index.html', '.gitignore'))) {
            $list[] = $file;
        }
    }
    closedir($handle);
    natcasesort($list);

    return $list;
}
